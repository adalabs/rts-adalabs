# rts-adalabs

Ada run-time dedicated to application-level test and simulation

## Project management

https://www.pivotaltracker.com/n/projects/1416758

## Features

- Ada.Calendar.Clock Simulated time with acceleration ratio

## Patches

- a-calend.adb
- a-caldel.adb
- a-reatim.adb
- a-rttiev.adb
- s-tasren.adb

## New files

- a-calext.adb
- a-calext.ads
- a-rtteex.ads

## Install

See `Makefile`

```
$ make build-gnat-gpl-2021
# make install-gnat-gpl-2021
```

You can try an install by doing

```
$ make build-gnat-gpl-2021
$ make install-gnat-gpl-2021 DESTDIR=/tmp OWNER=$USER GROUP=$USER
```


## Know issues

- [#180432788](https://www.pivotaltracker.com/story/show/180432788) gnat-gpl-2021/include should be adapted accordingly

## Update from a new GNAT run-time

- Initiate the new run-time in `rts-adalabs/new-runtime-version`
- Update Makefile as internal GCC version may have changed
- Update `adainclude` folder
  - Copy (`cp -p`) `file` to be patched to `file.orig`
  - Copy (`cp -p`) additional files present in `rts-adalabs/sources`
  - Update patches (from previous runtime to file of the latest run-time using `meld`
- Update `adainclude/libgnat.lst` with new files from `rts-adalabs/sources`
