-------------------------------------------------------------------------------
-- AUnit-Extras, Copyright (C) 2013-2015, AdaLabs Ltd                        --
--                                                      http://adalabs.com   --
-------------------------------------------------------------------------------
with System.OS_Primitives;

with Ada.Unchecked_Conversion;

package body Ada.Calendar.Extras is

   --------------------------
   -- Leap seconds control --
   --------------------------

   Flag : Integer;
   pragma Import (C, Flag, "__gl_leap_seconds_support");
   --  This imported value is used to determine whether the compilation had
   --  binder flag "-y" present which enables leap seconds. A value of zero
   --  signifies no leap seconds support while a value of one enables support.

   Leap_Support : constant Boolean := (Flag = 1);
   --  Flag to controls the usage of leap seconds in all Ada.Calendar routines

   Leap_Seconds_Count : constant Natural := 27;

   ---------------------
   -- Local Constants --
   ---------------------

   --  Lower and upper bound of Ada time. The zero (0) value of type Time is
   --  positioned at year 2150. Note that the lower and upper bound account
   --  for the non-leap centennial years.

   Ada_Low  : constant Time_Rep := -(61 * 366 + 188 * 365) * Nanos_In_Day;
   Ada_High : constant Time_Rep :=  (60 * 366 + 190 * 365) * Nanos_In_Day;

   --  Even though the upper bound of time is 2399-12-31 23:59:59.999999999
   --  UTC, it must be increased to include all leap seconds.

   --  Two constants used in the calculations of elapsed leap seconds.
   --  End_Of_Time is later than Ada_High in time zone -28. Start_Of_Time
   --  is earlier than Ada_Low in time zone +28.

   End_Of_Time   : constant Time_Rep :=
     Ada_High + Time_Rep (3) * Nanos_In_Day;
   Start_Of_Time : constant Time_Rep :=
     Ada_Low - Time_Rep (3) * Nanos_In_Day;

   --  The Unix lower time bound expressed as nanoseconds since the start of
   --  Ada time in UTC.

   Unix_Min : constant Time_Rep :=
     Ada_Low + Time_Rep (17 * 366 + 52 * 365) * Nanos_In_Day;

   --  The Unix upper time bound expressed as nanoseconds since the start of
   --  Ada time in UTC.

   --  The following table contains the hard time values of all existing leap
   --  seconds. The values are produced by the utility program xleaps.adb. This
   --  must be updated when additional leap second times are defined.

   Leap_Second_Times : constant array (1 .. Leap_Seconds_Count) of Time_Rep :=
     (-5601484800000000000,
      -5585587199000000000,
      -5554051198000000000,
      -5522515197000000000,
      -5490979196000000000,
      -5459356795000000000,
      -5427820794000000000,
      -5396284793000000000,
      -5364748792000000000,
      -5317487991000000000,
      -5285951990000000000,
      -5254415989000000000,
      -5191257588000000000,
      -5112287987000000000,
      -5049129586000000000,
      -5017593585000000000,
      -4970332784000000000,
      -4938796783000000000,
      -4907260782000000000,
      -4859827181000000000,
      -4812566380000000000,
      -4765132779000000000,
      -4544207978000000000,
      -4449513577000000000,
      -4339180776000000000,
      -4244572775000000000,
      -4197052774000000000);

   function Time_Rep_To_Duration is
     new Ada.Unchecked_Conversion (Time_Rep, Duration);
   --  Convert a time representation value into a duration value

   procedure Cumulative_Leap_Seconds
     (Start_Date    : Time_Rep;
      End_Date      : Time_Rep;
      Elapsed_Leaps : out Natural;
      Next_Leap     : out Time_Rep);
   --  Elapsed_Leaps is the sum of the leap seconds that have occurred on or
   --  after Start_Date and before (strictly before) End_Date. Next_Leap_Sec
   --  represents the next leap second occurrence on or after End_Date. If
   --  there are no leaps seconds after End_Date, End_Of_Time is returned.
   --  End_Of_Time can be used as End_Date to count all the leap seconds that
   --  have occurred on or after Start_Date.
   --
   --  Note: Any sub seconds of Start_Date and End_Date are discarded before
   --  the calculations are done. For instance: if 113 seconds is a leap
   --  second (it isn't) and 113.5 is input as an End_Date, the leap second
   --  at 113 will not be counted in Leaps_Between, but it will be returned
   --  as Next_Leap_Sec. Thus, if the caller wants to know if the End_Date is
   --  a leap second, the comparison should be:
   --
   --     End_Date >= Next_Leap_Sec;
   --
   --  After_Last_Leap is designed so that this comparison works without
   --  having to first check if Next_Leap_Sec is a valid leap second.

   function Duration_To_Time_Rep is
     new Ada.Unchecked_Conversion (Duration, Time_Rep);
   --  Convert a duration value into a time representation value

   -----------
   -- Real_Clock --
   -----------

   function Real_Clock return Time is
      Elapsed_Leaps : Natural;
      Next_Leap_N   : Time_Rep;

      --  The system clock returns the time in UTC since the Unix Epoch of
      --  1970-01-01 00:00:00.0. We perform an origin shift to the Ada Epoch
      --  by adding the number of nanoseconds between the two origins.

      Res_N : Time_Rep :=
        Duration_To_Time_Rep (System.OS_Primitives.Clock) + Unix_Min;

   begin
      --  If the target supports leap seconds, determine the number of leap
      --  seconds elapsed until this moment.

      if Leap_Support then
         Cumulative_Leap_Seconds
           (Start_Of_Time, Res_N, Elapsed_Leaps, Next_Leap_N);

         --  The system clock may fall exactly on a leap second

         if Res_N >= Next_Leap_N then
            Elapsed_Leaps := Elapsed_Leaps + 1;
         end if;

      --  The target does not support leap seconds

      else
         Elapsed_Leaps := 0;
      end if;

      Res_N := Res_N + Time_Rep (Elapsed_Leaps) * Nano;

      return Time (Res_N);
   end Real_Clock;

   -----------------------------
   -- Cumulative_Leap_Seconds --
   -----------------------------

   procedure Cumulative_Leap_Seconds
     (Start_Date    : Time_Rep;
      End_Date      : Time_Rep;
      Elapsed_Leaps : out Natural;
      Next_Leap     : out Time_Rep)
   is
      End_Index   : Positive;
      End_T       : Time_Rep := End_Date;
      Start_Index : Positive;
      Start_T     : Time_Rep := Start_Date;

   begin
      --  Both input dates must be normalized to UTC

      pragma Assert (Leap_Support and then End_Date >= Start_Date);

      Next_Leap := End_Of_Time;

      --  Make sure that the end date does not exceed the upper bound
      --  of Ada time.

      if End_Date > Ada_High then
         End_T := Ada_High;
      end if;

      --  Remove the sub seconds from both dates

      Start_T := Start_T - (Start_T mod Nano);
      End_T   := End_T   - (End_T   mod Nano);

      --  Some trivial cases:
      --                     Leap 1 . . . Leap N
      --  ---+========+------+############+-------+========+-----
      --     Start_T  End_T                       Start_T  End_T

      if End_T < Leap_Second_Times (1) then
         Elapsed_Leaps := 0;
         Next_Leap     := Leap_Second_Times (1);
         return;

      elsif Start_T > Leap_Second_Times (Leap_Seconds_Count) then
         Elapsed_Leaps := 0;
         Next_Leap     := End_Of_Time;
         return;
      end if;

      --  Perform the calculations only if the start date is within the leap
      --  second occurrences table.

      if Start_T <= Leap_Second_Times (Leap_Seconds_Count) then

         --    1    2                  N - 1   N
         --  +----+----+--  . . .  --+-------+---+
         --  | T1 | T2 |             | N - 1 | N |
         --  +----+----+--  . . .  --+-------+---+
         --         ^                   ^
         --         | Start_Index       | End_Index
         --         +-------------------+
         --             Leaps_Between

         --  The idea behind the algorithm is to iterate and find two
         --  closest dates which are after Start_T and End_T. Their
         --  corresponding index difference denotes the number of leap
         --  seconds elapsed.

         Start_Index := 1;
         loop
            exit when Leap_Second_Times (Start_Index) >= Start_T;
            Start_Index := Start_Index + 1;
         end loop;

         End_Index := Start_Index;
         loop
            exit when End_Index > Leap_Seconds_Count
              or else Leap_Second_Times (End_Index) >= End_T;
            End_Index := End_Index + 1;
         end loop;

         if End_Index <= Leap_Seconds_Count then
            Next_Leap := Leap_Second_Times (End_Index);
         end if;

         Elapsed_Leaps := End_Index - Start_Index;

      else
         Elapsed_Leaps := 0;
      end if;
   end Cumulative_Leap_Seconds;

   ----------------------
   -- Delay_Operations --
   ----------------------

   package Delay_Operations is

      function To_Duration (Date : Time) return Duration;

   end Delay_Operations;

   package body Delay_Operations is

      -----------------
      -- To_Duration --
      -----------------

      function To_Duration (Date : Time) return Duration is
         pragma Unsuppress (Overflow_Check);

         Safe_Ada_High : constant Time_Rep := Ada_High - Epoch_Offset;
         --  This value represents a "safe" end of time. In order to perform a
         --  proper conversion to Unix duration, we will have to shift origins
         --  at one point. For very distant dates, this means an overflow check
         --  failure. To prevent this, the function returns the "safe" end of
         --  time (roughly 2219) which is still distant enough.

         Elapsed_Leaps : Natural;
         Next_Leap_N   : Time_Rep;
         Res_N         : Time_Rep;

      begin
         Res_N := Time_Rep (Date);

         --  Step 1: If the target supports leap seconds, remove any leap
         --  seconds elapsed up to the input date.

         if Leap_Support then
            Cumulative_Leap_Seconds
              (Start_Of_Time, Res_N, Elapsed_Leaps, Next_Leap_N);

            --  The input time value may fall on a leap second occurrence

            if Res_N >= Next_Leap_N then
               Elapsed_Leaps := Elapsed_Leaps + 1;
            end if;

         --  The target does not support leap seconds

         else
            Elapsed_Leaps := 0;
         end if;

         Res_N := Res_N - Time_Rep (Elapsed_Leaps) * Nano;

         --  Step 2: Perform a shift in origins to obtain a Unix equivalent of
         --  the input. Guard against very large delay values such as the end
         --  of time since the computation will overflow.

         Res_N := (if Res_N > Safe_Ada_High then Safe_Ada_High
                                            else Res_N + Epoch_Offset);

         return Time_Rep_To_Duration (Res_N);
      end To_Duration;

   end Delay_Operations;

   -----------------
   -- To_Duration --
   -----------------

   function To_Duration (Date : Time) return Duration is
   begin
      return Delay_Operations.To_Duration (Date);
   end To_Duration;

end Ada.Calendar.Extras;
