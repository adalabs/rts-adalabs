-------------------------------------------------------------------------------
-- AUnit-Extras, Copyright (C) 2013-2015, AdaLabs Ltd                        --
--                                                      http://adalabs.com   --
-------------------------------------------------------------------------------

package Ada.Calendar.Extras is

   type Delay_Kind_Type is (Run_Time_Delay,
                            Acceleration_Ratio_Delay,
                            Null_Delay);

   type Clock_Kind_Type is (Acceleration_Ratio_Clock,
                            Run_Time_Clock);

   Prohibe_Use_Of_Realtime_Clock : Boolean := False;

   Delays_Kind        : Delay_Kind_Type := Run_Time_Delay;
   Clock_Kind         : Clock_Kind_Type := Run_Time_Clock;
   Accelaration_Ratio : Duration := 1.0;

   Current_Clock      : Ada.Calendar.Time := Ada.Calendar.Clock;
   pragma Atomic (Current_Clock);

   Epoch_Clock        : Ada.Calendar.Time := Ada.Calendar.Clock;
   pragma Atomic (Epoch_Clock);

   function Real_Clock return Time;

   function To_Duration (Date : Time) return Duration;

end Ada.Calendar.Extras;
