package Ada.Real_Time.Timing_Events.Extras
is
   Clock_Context_Id : Positive := 1;
   pragma Atomic (Clock_Context_Id);
end Ada.Real_Time.Timing_Events.Extras;
