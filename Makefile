DESTDIR?=
OWNER?=root
GROUP?=root

build-gnat-gpl-2021:
	make -C gnat-gpl-2021 build

install-gnat-gpl-2021:
	make -C gnat-gpl-2021 install DESTDIR=$(DESTDIR) OWNER=$(OWNER) GROUP=$(GROUP)

clean:
	make -C gnat-gpl-2021 clean
